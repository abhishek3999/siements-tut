export interface Person {
    id: number;
    name: string;
    address: string;
    company: string;
    restore?:any;
}