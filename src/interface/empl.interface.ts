export class Employee {
    id?: any;
    name: string = '';
    address: any = {
        // street: '',
        // suite: '',
        // city: '',
        // zipcode: ''
    };
    email: any;
    phone: any;
    website: any;
    company: any = {
        name: '',
        catchPhrase: '',
        bs: ''
    }
}